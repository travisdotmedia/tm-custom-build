

(function($) {

  // Search toggle
	$('.search-toggle').click(function(e){
		e.preventDefault();
		$('.header-search-form').toggleClass('search-open').find('input[type="search"]').focus();
	});
	$('.search-submit').click(function(e){
		if( $(this).parent().find('.search-field').val() == '' ) {
			e.preventDefault();
			$(this).parent().parent().removeClass('active');
		}

  });

  $('#nav-toggle').sidr({
    name: 'sidr',
    source: '#menu-primary-menu',
		side: 'right',
	});
	
	$(".content-subscribe-button").click(function(event) {
		$("#gform_wrapper_19").slideToggle();
		$("input_19_1_3").focus();
		$(".content-signup .content-cta").css("border-bottom","none");
		event.preventDefault();
	});

	// $(".cf-close-div").click(function(event) {
	// 	$(".notification-bar").hide();
	// 	$(".single .site-inner").css("margin-top", "120px");
	// 	$(".header-search-form").css("top", "80px");
	// });

	// if ($('.notification-bar').length) {
	// 	if ($('body').hasClass('single')) {
	// 			$(".single .site-inner").css("margin-top", "63px");
	// 	}
	// 	$(".header-search-form").css("top", "137px");
	// }
	
	if ($('body').hasClass('single')) {
		var articleHeight = $('.single .entry').height();
		var articleHeightAdjusted = ((articleHeight / 2) - 1046);

		if ( articleHeight > 2700)  {
			$('#widget_thrive_leads-2').css('margin-top', (articleHeightAdjusted + 'px'));
			$('.skillshare-banner').css('display', 'block');
		}
	}	

})( jQuery );

jQuery(function($) {
	$('.post').matchHeight(); 
});

//Sets equal column heights and readjusts responsively
// (function($) {
// 	function equalHeights (minHeight, maxHeight) {
// 		tallest = (minHeight) ? minHeight : 0;
// 		this
// 		.height('auto')
// 		.each(function() {
// 			if($(this).height() > tallest) {
// 				tallest = $(this).height();
// 			}
// 		});
// 		if((maxHeight) && tallest > maxHeight) tallest = maxHeight;
// 		return this.each(function() {
// 			$(this).height(tallest).css("overflow","visible");
// 		});
// 	}
// 	$.fn.equalHeights = function(minHeight, maxHeight) {
// 		var $this = this;
// 		$(window).resize(function(){
// 			equalHeights.call($this, minHeight, maxHeight);
// 		});
// 		equalHeights.call($this, minHeight, maxHeight);
// 		return this;
// 	};
// 	$(document).ready(function() {
// 		//Set objects here
// 		$(".blog article").equalHeights();
// 		$("#search-results article").equalHeights();
// 	});
// })(jQuery);


