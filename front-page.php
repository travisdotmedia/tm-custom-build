
<?php
/**
 * TMStarter
 *
 * @package      TMStarter
 * @license      GPL-2.0+
 */
// Remove 'site-inner' from structural wrap
add_theme_support('genesis-structural-wraps', array('header', 'footer-widgets', 'footer'));

//Add H1 to homepage
function tm_h1_for_site_title($wrap) {
  return 'h1';
}
add_filter('genesis_site_title_wrap', 'tm_h1_for_site_title');

/**
 * Add the attributes from 'entry', since this replaces the main entry
 *
 * @param array $attributes Existing attributes.
 * @return array Amended attributes.
 */
function tm_site_inner_attr($attributes) {

  // Add a class of 'full' for styling this .site-inner differently
  $attributes['class'] .= ' full';

  // Add an id of 'genesis-content' for accessible skip links
  $attributes['id'] = 'genesis-content';

  // Add the attributes from .entry, since this replaces the main entry
  $attributes = wp_parse_args($attributes, genesis_attributes_entry(array()));

  return $attributes;
}
add_filter('genesis_attr_site-inner', 'tm_site_inner_attr');
// Build the page
get_header();

?>
<section id="home-unique" class="tm-section">
  <div class="container aligncenter clearfix">
    <div class="unique-one one-third first">
    <img alt="people shaking hands" src="https://travis.media/wp-content/uploads/2018/04/one-to-one-200x200.png">
      <div class="unique-title">
      <h3>One-on-One</h3>
      </div>
      <div class="unique-text">
        <p>
        No jumping from designer to developer to digital strategist. It's one on one, me and you, the entire project, from discovery to deployment.
        </p>
      </div>
    </div>
    <div class="unique-one one-third">
    <img alt="tailored web design image" src="https://travis.media/wp-content/uploads/2018/04/tailored-web-design-200x200.png">
      <div class="unique-title">
      <h3>Tailored Service</h3>
      </div>
      <div class="unique-text">
        <p>
        All websites are uniquely designed around your specific business goals and developed for long-term success, from quality code to ease of use.
        </p>
      </div>
    </div>
    <div class="unique-one one-third">
    <img alt="solution driven image" src="https://travis.media/wp-content/uploads/2018/04/solution-driven-web-design-200x200.png">
      <div class="unique-title">
      <h3>Solution Driven</h3>
      </div>
      <div class="unique-text">
        <p>
        These days anyone can "build" a website, but happens next? I build sites to solve problems and provide solutions.
        </p>
      </div>
    </div>
  </div>
</section>

<section id="work-testimonials" class="tm-section">
  <div class="container clearfix">
    <div class="work-section clearfix">
      <div class="home-work-header">
          <h3 class="home-work-title">What I Do</h3>
      </div>
      <hr class="work-line">
      <div class="home-work one-half first">
        <div class="home-work-body">
            <p>I am a WordPress web designer & developer in Lynchburg,Virginia serving the local community and beyond.</p>
            <p>My passion is designing and developing custom websites that bring real solutions for small businesses and entrepreneurs...solutions that come from focused market research, strategic design, and optimal performance.</p>
            <p>Being a one-man show, I am able to work with each client personally from start to finish assuring a continuous, more consistent process both logically and creatively.</p>
            <p>Whether it is a new build, a redesign, or a custom solution, I look forward to working together with you.</p>
        </div>
      </div>
      <div class="home-work-project one-half">
        <h3 class="aligncenter full">A Recent Project</h3>
        <img alt="web design example" src="https://travis.media/wp-content/uploads/2018/04/web-design-example-appalachian-550x400.png">
      </div>
    </div>
    <div class="testimonials-section clearfix">
      <div class="clearfix home-testimonials-header">
          <h3 class="home-work-title alignright">What They Say</h3>
      </div>
      <hr class="testimonial-line">
      <div class="clearfix">
        <?php echo do_shortcode('[testimonial_view id=1]') ?>
      </div>
    </div>
  </div>
</section>

<section id="home-options" class="tm-section">
  <div class="container clearfix">
    <div class="tm-section-title aligncenter">
      <h3>So How Can I Assist You?</h3>
    </div>
    <div class="tm-section-content">
      <div class="option-one one-fourth first aligncenter">
        <p class="option-title">From Scratch</p>
        <img class="option-image aligncenter blue-dashed size-thumbnail" alt="gear vector image" src="https://travis.media/wp-content/uploads/2018/04/gear-vector-blue-150x150.png" />
        <p class="option-description">From design to development to deployment</p>
        <div class="box-tm">
          <a class="btn btn-tm" href="https://travis.media/what-i-do">
            <span>More Info</span>
          </a>
        </div>
      </div>
      <div class="option-two one-fourth aligncenter">
        <p class="option-title">Already Designed</p>
        <img class="option-image aligncenter orange-dashed size-thumbnail" alt="browser vector image" src="https://travis.media/wp-content/uploads/2018/04/browser-vector-orange-150x150.png" />
        <p class="option-description">You already have a design, you just need it built</p>
        <div class="box-tm">
          <a class="btn btn-tm" href="https://travis.media/contact">
            <span>Tell Me About It</span>
          </a>
        </div>
      </div>
      <div class="option-three one-fourth aligncenter">
        <p class="option-title">Custom Solutions</p>
        <img class="option-image aligncenter blue-dashed size-thumbnail" alt="code vector image" src="https://travis.media/wp-content/uploads/2018/04/code-vector-blue-150x150.png" />
        <p class="option-description">Custom coding and changes to your site</p>
        <div class="box-tm">
          <a class="btn btn-tm" href="https://travis.media/what-i-do">
            <span>Who I Work With</span>
          </a>
        </div>
      </div>
      <div class="option-four one-fourth aligncenter">
        <p class="option-title">Ongoing Support</p>
        <img class="option-image aligncenter orange-dashed size-thumbnail" alt="wrench vector image" src="https://travis.media/wp-content/uploads/2018/04/wrench-vector-orange-150x150.png" />
        <p class="option-description">Monthly maintenance to ensure optimal performance</p>
        <div class="box-tm">
          <a class="btn btn-tm" href="https://travis.media/website-care-plans">
            <span>Client Care Plans</span>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- <section id="home-cta" class="tm-section">
  <div class="container clearfix"></div>
</section> -->

<section id="home-panels" class="tm-section">
  <div class="container clearfix aligncenter">
    <div class="tm-section-title aligncenter">
      <h3>More</h3>
    </div>
    <div class="panel blog-panel one-third first">
      <a href="https://travis.media/blog"><img class="panel-image" alt="blog panel image" src="https://travis.media/wp-content/uploads/2018/04/blog-panel-300x300.jpg" />
        <div class="middle-button tm-box">
          <div class="middle-button-text btn btn-tm">Blog</div>
        </div>
      </a>
    </div>
    <div class="panel resources-panel one-third">
      <a href="https://travis.media/resources"><img class="panel-image" alt="resources panel image" src="https://travis.media/wp-content/uploads/2018/04/my-tools-panel-300x300.jpg" />
        <div class="middle-button tm-box">
          <div class="middle-button-text btn btn-tm">My Tools</div>
        </div>
      </a>
    </div>
    <div class="panel blog-panel one-third">
      <a href="https://travis.media/code"><img class="panel-image" alt="code panel image" src="https://travis.media/wp-content/uploads/2018/04/code-panel-300x300.jpg" />
        <div class="middle-button tm-box">
          <div class="middle-button-text btn btn-tm">Code</div>
        </div>
      </a>
    </div>
    <div class="panel contact-panel one-third first">
      <a href="https://travis.media/how-to-start-a-wordpress-blog-on-siteground"><img class="panel-image" alt="start a blog panel image" src="https://travis.media/wp-content/uploads/2018/04/start-a-blog-panel-300x300.jpg" />
        <div class="middle-button tm-box">
          <div class="middle-button-text btn btn-tm">Start a blog</div>
        </div>
      </a>
    </div>
    <div class="panel start-a-blog-panel one-third">
      <a href="https://travis.media/ebooks"><img class="panel-image" alt="ebooks panel image" src="https://travis.media/wp-content/uploads/2018/04/ebooks-panel-300x300.jpg" />
        <div class="middle-button tm-box">
          <div class="middle-button-text btn btn-tm">Ebooks</div>
        </div>
      </a>
    </div>
    <div class="panel videos-panel one-third">
      <a href="https://www.youtube.com/channel/UCGPGirOab9EGy7VH4IwmWVQ"><img class="panel-image" alt="ebooks panel image" src="https://travis.media/wp-content/uploads/2018/04/videos-panel-300x300.jpg" />
        <div class="middle-button tm-box">
          <div class="middle-button-text btn btn-tm">Videos</div>
        </div>
      </a>
    </div>

  </div>
</section>

<section id="home-contact" class="tm-section">
  <div class="container clearfix">
    <h3 class="contact-title aligncenter">Let's Talk</h3>
    <p class="aligncenter">Thanks for taking the time to contact me! This form is for all general inquiries, requests, feedback, etc., so fire away. But if you are looking to fill out the Website Project Questionnaire please <a href="https://travis.media/website-application">do so here</a>!!</p>
    <p><?php gravity_form(16, false, false, false, '', false);?></p>

  </div>
</section>



<?php
get_footer();