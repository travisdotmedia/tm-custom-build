<?php
/**
 * Asset loader handler.
 *
 * @package     TravisMedia\TMStarter
 * @since       1.0.0
 * @author      @travisdotmedia
 * @link        https://travis.media
 * @license     GNU General Public License 2.0+
 */
namespace TravisMedia\TMStarter;

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueue_assets' );
/**
 * Enqueue Scripts and Styles.
 *
 * @since 1.0.2
 *
 * @return void
 */
function enqueue_assets() {

	wp_enqueue_style( CHILD_TEXT_DOMAIN . '-fonts', '//fonts.googleapis.com/css?family=Merriweather:400,400i,700', array(), CHILD_THEME_VERSION );

	wp_enqueue_script ( CHILD_TEXT_DOMAIN . '-jquery.sidr.min' , CHILD_URL . '/assets/js/jquery.sidr.min.js', array( 'jquery' ), CHILD_THEME_VERSION, true );
	//wp_enqueue_script ( CHILD_TEXT_DOMAIN . '-sidr-init', CHILD_URL . '/assets/js/sidr-init.js', array( 'jquery.sidr.min' ), CHILD_THEME_VERSION, true );
	wp_enqueue_style ( CHILD_TEXT_DOMAIN . '-jquery.sidr.dark.css', CHILD_URL . '/assets/css/jquery.sidr.dark.css', '', CHILD_THEME_VERSION, 'all' );

	wp_enqueue_script ( CHILD_TEXT_DOMAIN . '-jquery.matchHeight.min' , CHILD_URL . '/assets/js/jquery.matchHeight.min.js', array( 'jquery' ), CHILD_THEME_VERSION, true );

	wp_enqueue_script( CHILD_TEXT_DOMAIN . '-custom.min', CHILD_URL . '/assets/js/custom.min.js', array( 'jquery' ), CHILD_THEME_VERSION, true );

	$localized_script_args = array(
		'mainMenu' => __( 'Menu', CHILD_TEXT_DOMAIN ),
		'subMenu'  => __( 'Menu', CHILD_TEXT_DOMAIN ),
	);
	wp_localize_script( CHILD_TEXT_DOMAIN . '-responsive-menu.min', 'tmstarterL10n', $localized_script_args );
}