<?php
/**
 * Setup your child theme
 *
 * @package     TravisMedia\TMStarter
 * @since       1.0.3
 * @author      @travisdotmedia
 * @link        https://travis.media
 * @license     GNU General Public License 2.0+
 */
namespace TravisMedia\TMStarter;

add_action( 'genesis_setup', __NAMESPACE__ . '\setup_child_theme', 15 );
/**
 * Setup child theme.
 *
 * @since 1.0.3
 *
 * @return void
 */

function setup_child_theme() {
	load_child_theme_textdomain( CHILD_TEXT_DOMAIN, apply_filters( 'child_theme_textdomain', CHILD_THEME_DIR . '/languages', CHILD_TEXT_DOMAIN ) );

	unregister_genesis_callbacks();

	adds_theme_supports();
	adds_new_image_sizes();
	genesis_unregister_layout( 'content-sidebar-sidebar' );	genesis_unregister_layout( 'sidebar-content-sidebar' );
	genesis_unregister_layout( 'sidebar-sidebar-content' );
	genesis_unregister_layout( 'sidebar-content' );
	remove_action( 'genesis_after_header', 'genesis_do_nav' );
	add_action( 'genesis_header_right', 'genesis_do_nav' );
	remove_action( 'genesis_before_loop', 'genesis_do_posts_page_heading' );
	remove_action( 'genesis_after_entry', 'genesis_do_author_box_single', 8 );
	remove_action( 'genesis_header', 'genesis_do_header' );
	add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );
	//add_action( 'genesis_header', 'tm_genesis_do_header' );
	remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
	add_action( 'wp_print_styles', 'tm_dequeue_dashicons_style' );
	add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
	add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );	
	add_filter( 'get_avatar_url', 'tm_avatar_remove_querystring' );
	genesis_register_sidebar( array(
		'id' => 'archive-widget',
		'name' => __( 'Archive Widget', 'genesis' ),
		'description' => __( 'Archive Widget Area', 'tmstarter' ),
	) );
	genesis_register_sidebar( array(
		'id' => 'affiliate-disclaimer',
		'name' => __( 'Affiliate Disclaimer', 'genesis' ),
		'description' => __( 'Affiliate Disclaimer Widget Area', 'tmstarter' ),
	) );
	// Register new sidebar
	genesis_register_sidebar( array(
		'id' => 'short-sidebar',
		'name' => 'Short Sidebar',
		'description' => 'This is the sidebar for short posts.',
	) );
	
	add_action ('genesis_before_footer','genesischild_footer_subscribe_position', 5);
	add_filter('script_loader_tag', 'clean_script_tag');
	add_action( 'wp_enqueue_scripts', 'child_manage_woocommerce_styles', 99 );
	add_action( 'wp_head', 'tm_google_tag_manager1' );
	add_action( 'genesis_before', 'tm_google_tag_manager2' );
	genesis_register_sidebar( array(
    'id' => 'notification-bar',
    'name' => __( 'Notification Bar', 'travismedia' ),
    'description' => __( 'This is the notification bar above the header.', 'travismedia' ),
	) );
	//add_action( 'genesis_before_header', 'notification_bar' );
	add_action( 'genesis_entry_header', 'tm_do_post_subtitle', 11 );
	add_action( 'woocommerce_checkout_process', 'tm_not_approved_privacy' );
	add_action( 'woocommerce_review_order_before_submit', 'tm_add_checkout_privacy_policy', 9 );
	add_filter( 'gform_submit_button_8', 'add_paragraph_below_submit', 10, 2 );

}


/**
 * Unregister Genesis callbacks.  We do this here because the child theme loads before Genesis.
 *
 * @since 1.0.0
 *
 * @return void
 */
function unregister_genesis_callbacks() {
	unregister_menu_callbacks();
}

/**
 * Adds theme supports to the site.
 *
 * @since 1.0.0
 *
 * @return void
 */
function adds_theme_supports() {
	$config = array(
		'html5'                           => array(
			'caption',
			'comment-form',
			'comment-list',
			'gallery',
			'search-form'
		),
		'genesis-accessibility'           => array(
			'404-page',
			'drop-down-menu',
			'headings',
			'rems',
			'search-form',
			'skip-links'
		),
		'genesis-responsive-viewport'     => null,
		// 'custom-header'                   => array(
		// 	'width'           => 600,
		// 	'height'          => 160,
		// 	'header-selector' => '.site-title a',
		// 	'header-text'     => false,
		// 	'flex-height'     => true,
		// ),
		'custom-background'               => null,
		'genesis-responsive-viewport'			=> null,
		'genesis-after-entry-widget-area' => null,
		'genesis-footer-widgets'          => 4,
		'genesis-menus'                   => array(
			'primary'   => __( 'After Header Menu', CHILD_TEXT_DOMAIN ),
			'secondary' => __( 'Footer Menu', CHILD_TEXT_DOMAIN ),
			'top' => __( 'Top Menu', CHILD_TEXT_DOMAIN )
		),
		'admin-bar' 											=> array( 
			'callback' => '__return_false' 
		),
	);

	foreach ( $config as $feature => $args ) {
		add_theme_support( $feature, $args );
	}
}

/**
 * Adds new image sizes.
 *
 * @since 1.0.0
 *
 * @return void
 */
function adds_new_image_sizes() {
	$config = array(
		'featured' => array(
			'width'  => 300,
			'height' => 100,
			'crop'   => true,
		),
		'blog-image' => array(
			'width'  => 768,
			'height' => 450,
			'crop'   => true,
		),
		'pinterest-image' => array(
			'width'  => 735,
			'height' => 1102,
			'crop'   => true,
		),
		'singular-featured-thumb' => array(
			'width'  => 768,
			'height' => 450,
			'crop'   => true,
		),
		'square-400' => array(
			'width'  => 400,
			'height' => 400,
			'crop'   => true,
		),
		'square-300' => array(
			'width'  => 300,
			'height' => 300,
			'crop'   => true,
		),
		'square-200' => array(
			'width'  => 200,
			'height' => 200,
			'crop'   => true,
		),
		'work-mockups' => array(
			'width'  => 550,
			'height' => 400,
			'crop'   => true,
		),
	);

	foreach( $config as $name => $args ) {
		$crop = array_key_exists( 'crop', $args ) ? $args['crop'] : false;

		add_image_size( $name, $args['width'], $args['height'], $crop );
	}
}

add_filter( 'genesis_theme_settings_defaults', __NAMESPACE__ . '\set_theme_settings_defaults' );
/**
 * Set theme settings defaults.
 *
 * @since 1.0.0
 *
 * @param array $defaults
 *
 * @return array
 */
function set_theme_settings_defaults( array $defaults ) {
	$config = get_theme_settings_defaults();

	$defaults = wp_parse_args( $config, $defaults );

	return $defaults;
}

add_action( 'after_switch_theme', __NAMESPACE__ . '\update_theme_settings_defaults' );
/**
 * Sets the theme setting defaults.
 *
 * @since 1.0.0
 *
 * @return void
 */
function update_theme_settings_defaults() {
	$config = get_theme_settings_defaults();

	if ( function_exists( 'genesis_update_settings' ) ) {
		genesis_update_settings( $config );
	}

	update_option( 'posts_per_page', $config['blog_cat_num'] );
}

/**
 * Get the theme settings defaults.
 *
 * @since 1.0.0
 *
 * @return array
 */
function get_theme_settings_defaults() {
	return array(
		'blog_cat_num'              => 9,
		'content_archive'           => 'full',
		'content_archive_limit'     => 0,
		'content_archive_thumbnail' => 0,
		'posts_nav'                 => 'numeric',
		'site_layout'               => 'content-sidebar',
	);
}

